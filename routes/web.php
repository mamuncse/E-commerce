<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'WelcomeController@index');

Route::get('/catagoryFront/{id}', 'WelcomeController@catagory');
Route::get('/contact', 'WelcomeController@contact');
Route::get('/product/details/{id}', 'WelcomeController@productDetails');
     /*CartDetails*/
Route::match(['get','post'],'/add_to_cart/{id}', 'cartController@cartDetails');
Route::get('/delete-from-cart/{id}', 'cartController@cartDelete');
Route::post('/cart/update', 'cartController@cartUpdate');
/*CartDetails*/
       /*checkout*/
Route::get('/checkout', 'checkoutController@index');
Route::post('/checkout/registration', 'checkoutController@customerRegister');
Route::get('/checkout/shipping','checkoutController@showShipping');
Route::post('/shipping/registration','checkoutController@shippingRegister');
Route::get('/checkout/payment','checkoutController@showPaymentForm');
Route::post('/checkout/order','checkoutController@saveOrderInfo');
Route::get('/checkout/my-home','checkoutController@customerHome');

     /*checkout*/



Auth::routes();

Route::group(['middleware' => 'AuthenticaleMiddleware'], function() {

    Route::get('/dashboard', 'dashboardController@index');
    /* Catagory Info */
    Route::get('/catagory/index', 'catagoryController@createCatagory');

    Route::post('/admin/Catagory/store', 'catagoryController@storeCatagory');
    Route::get('/catagory/manage', 'catagoryController@manageCatagory');
    Route::get('/Catagory/edit/{id}', 'catagoryController@editCatagory');
    Route::post('/Catagory/update', 'catagoryController@updateCatagory');
    Route::get('/Catagory/delete/{id}', 'catagoryController@deleteCatagory');
    Route::get('/published/catagory/{id}', 'catagoryController@publishedCatagory');
    Route::get('/unpublished/catagory/{id}', 'catagoryController@unpublishedCatagory');
   // Route::get('/catagory/paginate/','catagoryController@paginateCatagory');
  
    /* Catagory Info */

    /* Manufactural Info */
    Route::get('/manufactural/index', 'manufacturalController@createManufactural');
    Route::post('/admin/manufactural/store', 'manufacturalController@storeManufactural');
    Route::get('/manufactural/manage', 'manufacturalController@manageManufactural');
    Route::get('/Manufactural/edit/{id}', 'manufacturalController@editManufactural');
    Route::post('/Manufactural/update', 'manufacturalController@updateManufactural');
    Route::get('/Manufactural/delete/{id}', 'manufacturalController@deleteManufactural');
    Route::get('/Manufactural/published/{id}', 'manufacturalController@publishedManufactural');
    Route::get('/Manufactural/unpublished/{id}', 'manufacturalController@unpublishedManufactural');
    /* Manufactural Info */

    /* Product Info */
    Route::get('/product/menu', 'productController@createProduct');
    Route::post('/admin/product/store', 'productController@storeProduct');
    Route::get('/product/manage', 'productController@manageProduct');
    Route::get('/Product/views/{id}', 'productController@viewsProduct');
    Route::get('/Product/edit/{id}', 'productController@editProduct');
    Route::post('/product/update', 'productController@updateProduct');
    Route::get('/Product/delete/{id}', 'productController@deleteProduct');
    Route::get('/Product/published/{id}', 'productController@publishedProduct');
    Route::get('/Product/unpublished/{id}', 'productController@unpublishedProduct');
    /* Product Info */
    /* User Info */
    Route::get('/user/manage', 'userController@manageUser');

    /* User Info */
});
