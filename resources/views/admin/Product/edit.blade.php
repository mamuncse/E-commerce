
@extends('admin.layouts.master')
@section('title','add-product')
@section('content_title','Product Add')
@section('product_menu','active')
@section('description')
    <!-- Page Heading -->
     <div class="row">
          <h2 style="color:green; font-size:20px; text-align: center;">{{Session::get('message')}}</h2>
        <div class="col-lg-12">
            <h1 class="page-header">
                <small> </small>
            </h1>

        </div>
    </div>

    <div class="row">


     {!! Form::open(['url'=>'/product/update','method'=>'POST','enctype'=>'multipart/form-data','name'=>'productStatus']) !!}

            <div class="form-group">
                <label for="usr">Product Name:</label>
                <input type="text" name="productName" value="{{$productById->productName}}" class="form-control" id="usr">
                <input type="hidden" name="productId" value="{{$productById->id}}" class="form-control" id="usr">
                <span class="text-danger">{{$errors->has('productName')? $errors->first('productName'):''}}</span>
         
            </div>
            <div class="form-group">
                <select name="catagoryId" class="form-control">
                    <option>Select Catagory statuts</option>
                    @foreach($catagories as $catagory)
                    <option value="{{$catagory->id}}">{{$catagory->catagoryName}}</option>
                     @endforeach
                </select>
            </div>
               <div class="form-group">
                <select name="manufacturalId" class="form-control">
                    <option>Select manufactural statuts</option>
                    @foreach($manufacturals as $manufactural)
                    <option value="{{$manufactural->id}}">{{$manufactural->manufacturalName}}</option>
                     @endforeach
                </select>
            </div
            <div class="form-group">
                <label for="Prize">Product Prize:</label>
                <input type="number" name="productPrize" value="{{$productById->productPrize}}" class="form-control" id="Prize">
                <span class="text-danger">{{$errors->has('productPrize')? $errors->first('productPrize'):''}}</span>
            </div>
             <div class="form-group">
                <label for="Quantity">Product Quantity:</label>
                <input type="number" name="productQuantity" value="{{$productById->productQuantity}}" class="form-control" id="Quantity">
                <span class="text-danger">{{$errors->has('productQuantity')? $errors->first('productQuantity'):''}}</span>
            </div>
            <div class="form-group">
                <label for="Short">Product Short Description</label>
                <textarea class="form-control" rows="3" name="productShortDescription" id="Short">{{$productById->productShortDescription}}</textarea>
                <span class="text-danger">{{$errors->has('productShortDescription')? $errors->first('productShortDescription'):''}}</span>
            </div>
              <div class="form-group">
                <label for="Long">Product Long Description</label>
                <textarea class="form-control" rows="5" name="productLongDescription" id="Long">{{$productById->productLongDescription}}</textarea>
                <span class="text-danger">{{$errors->has('productLongDescription')? $errors->first('productLongDescription'):''}}</span>
            </div>
             <div class="form-group">
                <label for="Image">Product Image:</label>
                <input type="file" name="productImage" id="Image" accept="image/*">
                <img src="{{asset($productById->productImage)}}" alt="editImage" height="100" width="100">
                <span class="text-danger">{{$errors->has('productImage')? $errors->first('productImage'):''}}</span>
            </div>
            <div class="form-group">
                <select name="publicationStatus" class="form-control">
                    <option>Select publication statuts</option>
                    <option value="1">Publication</option>
                    <option value="0">Unpublication</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Update Product Info</button>
       {!! Form::close() !!}


        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    </div>
      <script>
        document.forms['productStatus'].elements['publicationStatus'].value={{$productById->publicationStatus}}
        document.forms['productStatus'].elements['catagoryId'].value={{$productById->catagoryId}}
        document.forms['productStatus'].elements['manufacturalId'].value={{$productById->manufacturalId}}
   </script>
@endsection

