@extends('admin.layouts.master')
@section('title','manage-product')
@section('content_title','Product Manage')

@section('product_list','active')

@section('description')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small></small>
            </h1>

        </div>
    </div>
 
    <div class="row">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Catagory Name</th>
                                <th>Manufactural Name</th>
                                <th>Product Prize</th>
                                <th>Product Quantity</th>
                                <th>Publication Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                            <tr>
                                  <td>{{$product->productName}}</td>
                                <td>{{$product->catagoryName}}</td>
                                <td>{{$product->manufacturalName}}</td>
                                  <td>TK. {{$product->productPrize}}</td>
                                    <td>{{$product->productQuantity}}</td>
                                <td>
                                    <?php if($product->publicationStatus==1){ ?>
                                    <span class="label label-success">Active</span>
                                    <?php }else{ ?>
                                    <span class="label label-primary">Inactive</span>
                                    <?php } ?>
                                  </td>
                                <td>
                                     <?php if($product->publicationStatus==1){ ?>
                                     <a href="{{url('/Product/published/'.$product->id)}}" class="btn btn-success">
                                        <span >Published</span>
                                    </a>
                                      <?php }else{ ?>
                                     <a href="{{url('/Product/unpublished/'.$product->id)}}" class="btn btn-primary">
                                        <span> Unpublished</span>
                                    </a>
                                          <?php } ?>
                                    <a href="{{url('/Product/views/'.$product->id)}}" class="btn btn-rounded" >
                                        <span>View</span>
                                    </a>
                                    <a href="{{url('/Product/edit/'.$product->id)}}" class="btn btn-info" title="ProductUpdate">
                                        <span>Update</span>
                                    </a>
                                    <a onclick="return confirm('Are you sure to Delete!!')"; href="{{url('/Product/delete/'.$product->id)}}"class="btn btn-danger" title="ProductDelete">
                                    <span >Delete</span>
                                    </a></td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
           
      
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    </div>

@endsection