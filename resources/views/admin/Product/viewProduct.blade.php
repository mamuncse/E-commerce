@extends('admin.layouts.master')
@section('title','Views-product')
@section('content_title','View Product')

@section('product_list','active')

@section('description')
<table class="table table-bordered table-hover">
    <tr>
        <th>Product ID</th>
        <th>{{$product->id}}</th>
    </tr>
    <tr>
        <th>Product Name</th>
        <th>{{$product->productName}}</th>
    </tr>
    <tr>
        <th>Catagory Name</th>
        <th>{{$product->catagoryName}}</th>
    </tr>
    <tr>
        <th>Manufactural Name</th>
        <th>{{$product->manufacturalName}}</th>
    </tr>
  <tr>
        <th>Product Prize</th>
        <th>{{$product->productPrize}}</th>
    </tr>
     <tr>
        <th>Product Quantity</th>
        <th>{{$product->productQuantity}}</th>
    </tr>
     <tr>
        <th>Product ShortDescription</th>
        <th>{!!$product->productShortDescription!!}</th>
    </tr>
     <tr>
        <th>Product LongDescription</th>
        <th>{!!$product->productLongDescription!!}</th>
    </tr>
     <tr>
        <th>Product Image</th>
        <th><img src="{{asset($product->productImage)}}" alt="{{$product->productName}}" height="150" width="150"></th>
    </tr>
     <tr>
        <th>Product PublicatinStatus</th>
        <th>{{$product->publicationStatus==1?'Published':'Unpublished'}}</th>
    </tr>
</table>
@endsection
