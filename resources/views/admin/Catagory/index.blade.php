@extends('admin.layouts.master')
@section('title','Add catagory')
@section('content_title','Catagory Add')

@section('catagory_add','active')

@section('description')
    <!-- Page Heading -->
    <div class="row">
    <h2 style="color:green; font-size:20px; text-align: center;">{{Session::get('message')}}</h2>
        <div class="col-lg-12">
    
            <h1 class="page-header">
                {!! Form::open(['url'=>'/admin/Catagory/store','method'=>'POST']) !!}
                {!! Form::label('catagoryName', 'Catagory Name',['class'=>'form-group']) !!}
                {!! Form::text('catagoryName',null,['class'=>'form-control']),$errors->has('catagoryName')? $errors->first('catagoryName'):'' !!}
                {!! Form::label('catagoryDescription', 'Catagory Description',['class'=>'form-group']) !!}
                {!! Form::textarea('catagoryDescription',null,['class'=>'form-control']),$errors->has('catagoryDescription')? $errors->first('catagoryDescription'):'' !!}
                {!! Form::select('publicationStatus',['1'=>'publication','0'=>'unpublication']) !!}
                {!! Form::submit('Save Catagory Info',['class'=>'btn bg-teal btn-block btn-lg']) !!}
                {!! Form::close() !!}
            </h1>

        </div>
    </div>


@endsection