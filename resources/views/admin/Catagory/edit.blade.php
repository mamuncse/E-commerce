@extends('admin.layouts.master')
@section('title','Add catagory')
@section('content_title','Catagory Add')

@section('catagory_add','active')

@section('description')



<!-- Page container -->
<div class="page-container">
   <h2 style="color:green; font-size:20px; text-align: center;">{{Session::get('message')}}</h2>
    <!-- Page content -->
    <div class="page-content">
        <!-- /page header -->
        <div class="block">
            {!! Form::open(['url'=>'/Catagory/update','method'=>'POST','name'=>'editCatagoryForm']) !!}
                <table class="form" id="table_post" class="form-horizontal">
                    <tr>
                        <td>
                            <label>Catagory Name</label>
                        </td>
                        <td>
                            <input type="text" class="medium" name="catagoryName" value="{{$catagoriesById->catagoryName}}"/>
                        </td>
                    </tr>
                  
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Catagory Description</label>
                        </td>
                        <td>
                            <textarea  rows="7" cols="50" name="catagoryDescription">{{$catagoriesById->catagoryDescription}}"</textarea>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label> Publicatin</label>
                        </td>
                        <td>
                            <select class="form-control" name="publicationStatus">
                                <option value="1">publication</option>
                                <option value="0" >unpublication</option>
                            </select>
                        </td>
                    </tr>
                    <tr>

                        <td>
                            <input type="hidden" name='id' value="{{$catagoriesById->id}}" class='form-control'/>
                            <input type="submit" Value="Update" class='btn bg-teal btn-block btn-lg'/>
                        </td>
                    </tr>
                </table>
     {!! Form::close() !!}
        </div>
        <script>
            document.forms['editCatagoryForm'].elements['publicationStatus'].value={{$catagoriesById->publicationStatus}}
           </script>

@endsection

