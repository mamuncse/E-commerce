@extends('admin.layouts.master')
@section('title','Add catagory')
@section('content_title','Catagory Manage')

@section('manage_catagory','active')

@section('description')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small></small>
            </h1>

        </div>
    </div>
 
    <div class="row">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="20%">Catagory Name</th>
                                <th width="40%">Catagory Description</th>
                                <th width="5%">Publication Status</th>
                                <th width="30%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($catagories as $catagory)
                            <tr>
                                  <td>{{$catagory->id}}</td>
                                <td>{{$catagory->catagoryName}}</td>
                                <td>{{$catagory->catagoryDescription}}</td>
                                <td>
                                <?php if ($catagory->publicationStatus == 1) { ?>
                                        <span class="label label-success">
                                            Active
                                        </span>
                                    <?php } else { ?>
                                        <span class="label label-danger">
                                            Inactive
                                        </span>
                                    <?php } ?>
                                    </td>
                                <td>
                                         <?php 
            if($catagory->publicationStatus==1){
            ?>
            <a href="{{url('/published/catagory/'.$catagory->id)}}" class="btn btn-success">
                <span title="Published">Published</span>
            </a>
            <?php }else{ ?>
            <a href="{{url('/unpublished/catagory/'.$catagory->id)}}" class="btn btn-primary">
                <span  title="Unpublished">Unpublished</span>
            </a>
            <?php }?>
                                    <a href="{{url('/Catagory/edit/'.$catagory->id)}}"class="btn btn-info">
                                        <span >Update</span>
                                    </a>
                                    <a onclick="return confirm('Are you sure to Delete!!')"; href="{{url('/Catagory/delete/'.$catagory->id)}}"class="btn btn-danger"/>
                                    <span>Delete</span>
                                    </a></td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
           
      
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    </div>

@endsection