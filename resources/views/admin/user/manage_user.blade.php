@extends('admin.layouts.master')
@section('title','Manage Users')
@section('content_title','Users Manage')

@section('user_list','active')

@section('description')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small style="color:blue;">
                   Total {{$allUsers->total()}} Users<br/>
                   Per page show {{$allUsers->count()}} users
                </small>
            </h1>

        </div>
    </div>
 
    <div class="row">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>   
                                <th width="20%">User ID</th>
                                <th width="20%">User Name</th>
                                <th width="40%">Address</th>
                                <th width="5%">Email</th>
                                <th width="30%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; ?>
                            @foreach($allUsers as $user)
                            <tr>
                                <td>{{$i++}}</td>  
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->address}}</td>
                                <td> {{$user->email}}</td>
                                <td><a href=""class="btn btn-success">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a onclick="return confirm('Are you sure to Delete!!')"; href=""class="btn btn-danger"/>
                                    <span class="glyphicon glyphicon-trash"></span>
                                    </a></td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                         {{$allUsers->links()}}
                    </div>
                </div>
           
      
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    </div>

@endsection

