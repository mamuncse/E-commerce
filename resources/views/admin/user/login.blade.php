<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin-login</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{asset('public/admin/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/pages/login.js')}}"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Page container -->
<div class="page-container login-container">
    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Advanced login -->
         {!! Form::open(['url'=>'/login','method'=>'POST']) !!}
                    <div class="panel panel-body login-form">

                        <div class="text-center">
                            <div class="icon-object border-slate-300 text-slate-300"></div>

                            <h5 class="content-group">Login to your account </h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? ' has-error' : '' }}">
                           {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'enter your valid email']) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('password') ? ' has-error' : '' }}">
                            {!! Form::password('password',['class'=>'form-control','placeholder'=>'enter your password']) !!}
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                      {!! Form::checkbox('name','rememberMe') !!}
                                        Remember Me
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="login_password_recover.php">Forgot password?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Login',['class'=>'btn bg-blue btn-block']) !!}
                        </div>

                        <div class="content-divider text-muted form-group"><span>Don't have an account?</span></div>
                        <a href="{{url('auth/register')}}" class="btn btn-default btn-block content-group">Sign
                            up</a>
                    </div>
       {!! Form::close() !!}
                <!-- /advanced login -->


                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2017. <a href="#">Code of Ethics</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>