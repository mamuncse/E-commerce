<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><i class="icon-menu" title="Main pages"></i></li>
            <li class="active"><a href="{{url('/dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
            <li>
                <a href="#"><i class="icon-stack2"></i> <span>Catagory Info</span></a>
                <ul>
                    <li class="@yield('catagory_add')"><a href="{!! url('/catagory/index')!!}">Add Catagory</a></li>
                    <li class="@yield('manage_catagory')"><a href="{{url('/catagory/manage')}}">Manage Catagory</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-cog5"></i> <span>Manufactural</span></a>
                <ul>
                    <li class="@yield('manufactural_add')"><a href="{{url('/manufactural/index')}}">Manufactural Add</a></li>
                    <li class="@yield('manufactural_manage')"><a href="{{url('/manufactural/manage')}}">Manufactural Manage</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-copy"></i> <span>Product Info</span></a>
                <ul>
                    <li class="@yield('product_menu')"><a href="{{url('/product/menu')}}" id="layout1">Product Add</a></li>
                    <li class="@yield('product_list')"><a href="{{url('/product/manage')}}" id="layout2">Product Manage</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-spell-check"></i> <span>User</span></a>
                <ul>
                   
                    <li class="@yield('user_list')"><a href="{{url('/user/manage')}}">Manage Users</a></li>

                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-copy"></i> <span>Facts</span></a>
                <ul>
                    <li class="@yield('facts_menu')"><a href="{{'/Admin/Facts'}}">New Add</a></li>
                    <li class="@yield('facts_list')"><a href="{{'/Admin/Facts/facts_list'}}">My Facts</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-menu"></i> <span>Awards</span></a>
                <ul>
                    <li class="@yield('awards_menu')"><a href="{{'/Admin/Awards'}}">New Add</a></li>
                    <li class="@yield('awards_list')"><a href="{{'/Admin/Awards/awards_list'}}">My Awards</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-select2"></i> <span>Portfolios</span></a>
                <ul>
                    <li class="@yield('portfolios_menu')"><a href="{{'/Admin/Portfolios'}}">New Add</a></li>
                    <li class="@yield('portfolios_list')"><a href="{{'/Admin/Portfolios/portfolios_list'}}">My Portfolios</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Service</span></a>
                <ul>
                    <li class="@yield('service_menu')"><a href="{{'/Admin/Service'}}">New Add</a></li>
                    <li class="@yield('service_list')"><a href="{{'/Admin/Service/service_list'}}">My Service</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-footprint"></i> <span>Experience</span></a>
                <ul>
                    <li class="@yield('experience_menu')"><a href="{{'/Admin/Experience'}}">New Add</a></li>
                    <li class="@yield('experience_list')"><a href="{{'/Admin/Experience/experience_list'}}">My Experience</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-menu"></i> <span>Skills</span></a>
                <ul>
                    <li class="@yield('skills_menu')"><a href="{{'/Admin/Skills'}}">New Add</a></li>
                    <li class="@yield('skills_list')"><a href="{{'/Admin/Skills/skills_list'}}">My Skills</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-insert-template"></i> <span>Posts</span></a>
                <ul>
                    <li class="@yield('posts_menu')"><a href="{{'/Admin/Posts'}}">New Add</a></li>
                    <li class="@yield('posts_list')"><a href="{{'/Admin/Posts/posts_list'}}">My Posts</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-people"></i> <span>Contact</span></a>
                <ul>
                    <li class="@yield('contact_menu')"><a href="{{'/Admin/Contact'}}">New Add</a></li>
                    <li class="@yield('contact_list')"><a href="{{'/Admin/Contact/contact_list'}}">My Contact</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>