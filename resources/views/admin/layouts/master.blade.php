<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') BigBazar</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/admin/assets/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script type="text/javascript" src="{{asset('public/admin/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/assets/js/pages/dashboard.js')}}"></script>
    <!-- /theme JS files -->
       <script type="text/javascript" src="{{asset('public/admin/tinymce/jquery.tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/admin/tinymce/tinymce.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/admin/tinymce/tinymce.min.js')}}"></script>

</head>

<body>

<!-- Main navbar -->
@include('Admin.layouts.inc.header')
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="{{asset('images/mamun.jpg')}}" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">{{isset(Auth::user()->name) ? Auth::user()->name:'NULL' }}</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Dhaka,bd
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
            @include('Admin.layouts.inc.sidebarMenu')
            <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
        @include('Admin.layouts.inc.pageHeader')
        <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Main charts -->
                <div class="row">
                    <div class="col-lg-7">

                        <!-- Traffic sources -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title" width="700px" style="color:forestgreen">@yield('content_title')</h6>
                                <hr/>
                                @yield('description')
                                <div class="heading-elements">

                                </div>
                            </div>

                            <div class="container-fluid">

                            </div>

                        </div>
                        <!-- /traffic sources -->

                    </div>

                </div>
                <!-- /main charts -->

                <!-- Footer -->
            @include('Admin.layouts.inc.footer')
            <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
