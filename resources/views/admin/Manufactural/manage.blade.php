@extends('admin.layouts.master')
@section('title','manage-Manufactural')
@section('content_title','Manufactural Manage')

@section('manufactural_manage','active')

@section('description')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small></small>
            </h1>

        </div>
    </div>
 
    <div class="row">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="20%">Manufactural Name</th>
                                <th width="40%">Manufactural Description</th>
                                <th width="5%">Publication Status</th>
                                <th width="30%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($manufacturals as $manufactural)
                            <tr>
                                  <td>{{$manufactural->id}}</td>
                                <td>{{$manufactural->manufacturalName}}</td>
                                <td>{{$manufactural->manufacturalDescription}}</td>
                                <td>
                                    <?php if($manufactural->publicationStatus==1){ ?>
                                    <span class="label label-success">Active</span>
                                    <?php }else{ ?>
                                    <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                         </td>
                                <td>
                                     <?php if($manufactural->publicationStatus==1){ ?>
                                    <a href="{{url('/Manufactural/published/'.$manufactural->id)}}"class="btn btn-success">
                                        <span >Published</span>
                                    </a>
                                        <?php }else{ ?>
                                    <a href="{{url('/Manufactural/unpublished/'.$manufactural->id)}}"class="btn btn-primary">
                                        <span > Unpublished</span>
                                    </a>
                                         <?php } ?>
                                    <a href="{{url('/Manufactural/edit/'.$manufactural->id)}}"class="btn btn-info">
                                        <span >Update</span>
                                    </a>
                                    <a onclick="return confirm('Are you sure to Delete!!')"; href="{{url('/Manufactural/delete/'.$manufactural->id)}}"class="btn btn-danger"/>
                                    <span>Delete</span>
                                    </a></td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
           
      
    </div>
    <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    </div>

@endsection

