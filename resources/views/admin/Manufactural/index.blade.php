@extends('admin.layouts.master')
@section('title','Create-manufactural')
@section('content_title','Manufactural Add')
@section('manufactural_add','active')
@section('description')
    <!-- Page Heading -->
    <div class="row">
    <h2 style="color:green; font-size:20px; text-align: center;">{{Session::get('message')}}</h2>
        <div class="col-lg-12">
    
            <h1 class="page-header">
                {!! Form::open(['url'=>'/admin/manufactural/store','method'=>'POST']) !!}
                {!! Form::label('manufacturalName', 'manufactural Name',['class'=>'form-group']) !!}
                {!! Form::text('manufacturalName',null,['class'=>'form-control']),$errors->has('manufacturalName')? $errors->first('manufacturalName'):'' !!}
                {!! Form::label('manufacturalDescription', 'Manufactural Description',['class'=>'form-group']) !!}
                {!! Form::textarea('manufacturalDescription',null,['class'=>'form-control']),$errors->has('manufacturalDescription')? $errors->first('manufacturalDescription'):'' !!}
                {!! Form::select('publicationStatus',['1'=>'publication','0'=>'unpublication']) !!}
                {!! Form::submit('Save Manufactural Info',['class'=>'btn bg-teal btn-block btn-lg']) !!}
                {!! Form::close() !!}
            </h1>

        </div>
    </div>
@endsection

