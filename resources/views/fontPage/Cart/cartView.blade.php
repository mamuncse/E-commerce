@extends('fontPage.layouts.master')
@section('title','cart-add')
@section('main_contain')
<div class="checkout">
    <div class="container">
        <h3>My Shopping Bag</h3>
        <div class="table-responsive checkout-right animated wow slideInUp" data-wow-delay=".5s">
            <table class="timetable_sub">
                <thead>
                    <tr>
                        <th>Remove</th>
                        <th>Product Image</th>
                        <th>Quantity</th>
                        <th>Product Name</th>
                        <th>Price</th>
                         <th>Total</th>
                    </tr>
                </thead>
                <?php
                $contents=Cart::content();
//                print_r($contents);
//                die();
                foreach ($contents as $content){
                ?>
                <tr class="rem1">
                    <td class="invert-closeb">
                        <div class="rem">
                            <a href="{{url('/delete-from-cart/'.$content->rowId)}}"> <div class="close1"> </div></a>
                        </div>
                        <script>$(document).ready(function (c) {
                                $('.close1').on('click', function (c) {
                                    $('.rem1').fadeOut('slow', function (c) {
                                        $('.rem1').remove();
                                    });
                                });
                            });
                        </script>
                    </td>
                    <td class="invert-image"><a href="single.html"><img src="{{asset($content->options['image'])}}" alt=" " class="img-responsive" /></a></td>
                    <td class="invert">
                        <div class="quantity"> 
                            <div class="quantity-select">                           
<!--                                <div class="entry value-minus">&nbsp;</div>
                       
                                
                                <div class="entry value"><span>{{$content->qty}}</span></div>
                                <div class="entry value-plus active">&nbsp;</div>-->
                                     {!! Form::open(['url'=>'/cart/update','method'=>'POST']) !!}
                                   
                                <input type="text" name="qty" value="{{$content->qty}}">
                                <input type="hidden" name="rowId" value="{{$content->rowId}}">
                                <input type="submit" value="Update">
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </td>
                    <td class="invert">{{$content->name}}</td>
                    <td class="invert">{{$content->price}}</td>
                     <td class="invert">{{$content->qty*$content->price}}</td>
                </tr>
            
                
<?php } ?>
                <!--quantity-->
                <script>
//                    $('.value-plus').on('click', function () {
//                        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
//                        divUpd.text(newVal);
//                    });
//
//                    $('.value-minus').on('click', function () {
//                        var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
//                        if (newVal >= 1)
//                            divUpd.text(newVal);
//                    });
                </script>
                <!--quantity-->
            </table>
        </div>
        <div class="checkout-left">	

            <div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
                <a href="{{url('/')}}"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back To Shopping</a>
                <?php
                $customerId = Session::get('customerId');
                $shippingId = Session::get('shippingId');
                if($customerId !=null && $shippingId !=null ){?>
            
                <a style="color:green;" href="{{url('/checkout/payment')}}"><span style="color:yellow;"class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Checkout</a>
          <?php
                 }elseif ($customerId !=NULL) {
                ?>
                <a style="color:green;" href="{{url('/checkout/shipping')}}"><span style="color:yellow;" class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Checkout</a>
                 <?php }else{?>
                  <a style="color:green;" href="{{url('/checkout')}}"><span style="color:yellow;" class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Checkout</a>
                 <?php } ?>


            </div>
            <div class="checkout-left-basket animated wow slideInLeft" data-wow-delay=".5s">
                <h4>Shopping basket</h4>
                <ul>
                    <li>Vat(15%)<i>-</i> <span>BDT <?php 
                    $vat=(Cart::total(2,'.','')*15)/100;
                    echo $vat;
                            ?></span></li>
            
                   <li>Grand Total <i>-</i> <span><?php
                   $total=Cart::total(2,'.','');
                   $grandTotal=$vat+$total;
                   echo $grandTotal;
                   ?></span>
                   <?php
                   Session::put('orderTotal',$total)
                   ?>
                   </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
      
    </div>
</div>
@endsection

