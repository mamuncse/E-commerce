@include('fontPage.layouts.inc.head')

<!-- header -->
@include('fontPage.layouts.inc.header')
<!-- //header -->
<!-- header-bot -->
@include('fontPage.layouts.inc.header_bot')
<!-- //header-bot -->
<!-- banner -->
@include('fontPage.layouts.inc.banner')
<!-- //banner-top -->
<!-- banner -->
<!-- //banner -->
<!-- content -->
@yield('main_contain')
<!-- footer -->
@include('fontPage.layouts.inc.footer')
<!-- //footer -->
<!-- login -->
@include('fontPage.layouts.inc.footer_login')
<!-- //login -->
</body>
</html>
