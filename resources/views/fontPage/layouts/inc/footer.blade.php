<div class="footer">
    <div class="container">
        <div class="col-md-3 footer-left">
            <h2><a href="index.html"><img src="{{asset('public/fontPage/images/moder_bazar.png')}}" alt=" " /></a></h2>

        </div>
        <div class="col-md-9 footer-right">
            <div class="col-sm-6 newsleft">
                <h3>SIGN UP FOR NEWSLETTER !</h3>
            </div>
            <div class="col-sm-6 newsright">
                <form>
                    <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Submit">
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="sign-grds">
                <div class="col-md-4 sign-gd">
                    <h4>Information</h4>
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="mens.html">Men's Wear</a></li>
                        <li><a href="womens.html">Women's Wear</a></li>
                        <li><a href="electronics.html">Electronics</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>

                <div class="col-md-4 sign-gd-two">
                    <h4>Store Information</h4>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : 1234k Avenue, 4th block, <span>Dhaka City.</span></li>
                        <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:modern_bazar@gmail.com">modern_bazar@gmail.com</a></li>
                        <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : +8801915154954</li>
                    </ul>
                </div>
                <div class="col-md-4 sign-gd flickr-post">
                    <h4>Flickr Posts</h4>
                    <ul>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b15.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b16.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b17.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b18.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b15.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b16.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b17.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b18.jpg')}}" alt=" " class="img-responsive" /></a></li>
                        <li><a href="single.html"><img src="{{asset('public/fontPage/images/b15.jpg')}}" alt=" " class="img-responsive" /></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <p class="copy-right">&copy 2018. All rights reserved | Develop By <a href="http://techmamun/">Tech Mamun</a></p>
    </div>
</div>
