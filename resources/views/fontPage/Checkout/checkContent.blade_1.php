@extends('fontPage.layouts.master')
@section('title','cart-add')
@section('main_contain')

<div class="login">
    <div class="login-bottom">
        <h3>Registration Form Here</h3>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="well lead text-center text-success">
                        Your have login to complete your valuable order.If you are not registered then sign up first.
                    </div>
                </div>
            </div>
        </div>
      
            {!! Form::open(['url'=>'/checkout/registration','method'=>'POST']) !!}
                        <div class="form-group has-feedback has-feedback-left">
                             <span class="text-danger">{{$errors->has('fname')? $errors->first('fname'):''}}</span>
                            <input type="text" class="form-control" placeholder="Enter your FullName" name="fname">
                        
                        </div>
                      
                       <div class="form-group has-feedback has-feedback-left">
                              <span class="text-danger">{{$errors->has('email')? $errors->first('email'):''}}</span>
                            <input type="email" class="form-control" placeholder="Enter your Email" name="email">
                           
                        </div>
                <div class="form-group has-feedback has-feedback-left">
                      <span class="text-danger">{{$errors->has('password')? $errors->first('password'):''}}</span>
                            <input type="password" class="form-control" placeholder="Enter your password" name="password">
                           
                        </div>
                           <div class="form-group has-feedback has-feedback-left">
                                 <span class="text-danger">{{$errors->has('phone')? $errors->first('phone'):''}}</span>
                            <input type="number" class="form-control" placeholder="Enter your phone" name="phone">
                          
                        </div>
                           <div class="form-group has-feedback has-feedback-left">
                                  <span class="text-danger">{{$errors->has('address')? $errors->first('address'):''}}</span>
                               <textarea class="form-control" placeholder="Enter your Address" name="address"></textarea>
                         
                           </div>
                        <div class="form-group ">
                             <span class="text-danger">{{$errors->has('district')? $errors->first('district'):''}}</span>
                            <label>District Name </label>
                            <select class="form-control" name="district">
                                <option>Select District</option>
                                <option value="dhaka">Dhaka</option>
                                <option value="comilla">Comilla</option>
                                
                            </select>
                             
                        </div>

                        <button type="submit" class="btn btn-primary">Register</button>
                     
                    </div>
                {!! Form::close() !!}
<div class="login">
    <div class="login-bottom">
        <h3>Login Form Here</h3>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="well lead text-center text-success">
                        Your have login to Here
                    </div>
                </div>
            </div>
        </div>
                   {!! Form::open(['url'=>'/admin/Catagory/store','method'=>'POST']) !!}
                       
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="email" class="form-control" placeholder="Enter your email" name="email">
                            <div class="form-control-feedback">
                                <i class="icon-user-check text-muted"></i>
                            </div>
                        </div>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" class="form-control" placeholder="Enter your password" name="password">
                            <div class="form-control-feedback">
                                <i class="icon-user-check text-muted"></i>
                            </div>
                        </div>
                         <button type="submit" class="btn btn-primary">Login</button>  
            </h1>
                {!! Form::close() !!}
    </div>
</div>
</div>
        
 @endsection