@extends('fontPage.layouts.master')
@section('title','Shipping-add')
@section('main_contain')

<div class="login">
    <div class="login-bottom">
        <h3>Shipping Form Here</h3>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="well lead text-center text-success">
                        Hello <b>{{$customerById->fname}}</b>
                           Your have give us product shipping information to complete your valuable order.If your product billing information & shipping information are save then just press on shipping info button on  
                    </div>
                </div>
            </div>
        </div>
           {!! Form::open(['url'=>'/shipping/registration','method'=>'POST','name'=>'shippingForm']) !!}
                       <div class="form-group has-feedback has-feedback-left">
                           <input type="text" class="form-control" placeholder="Enter your FullName" name="fname" value="{{$customerById->fname}}">
                           
                        </div>
                      
                       <div class="form-group has-feedback has-feedback-left">
                            <input type="email" class="form-control" placeholder="Enter your Email" name="email"value="{{$customerById->email}}">
                            
                       </div>
                      
                           <div class="form-group has-feedback has-feedback-left">
                            <input type="number" class="form-control" placeholder="Enter your phone" name="phone" value="{{$customerById->phone}}">
                            
                        </div>
                           <div class="form-group">
                               <textarea class="form-control" placeholder="Enter your Address" name="address">{{$customerById->address}}"</textarea>
                           
                           </div>
                        <div class="form-group ">
                            <label>District Name </label>
                            <select class="form-control" name="district" >
                                <option>Select District</option>
                                <option value="dhaka">Dhaka</option>
                                <option value="comilla">Comilla</option>
                                
                            </select>
                        
                        </div>
               <button type="submit" class="btn btn-primary">Shipping save</button>
            </h1>
                {!! Form::close() !!}
    </div>
</div>
<script>
    document.forms['shippingForm'].elements['district'].value='{{$customerById->district}}';
    </script>
        <div class="coupons">
    <div class="container">
        <div class="coupons-grids text-center">
          
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
 @endsection

