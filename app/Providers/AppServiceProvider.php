<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Catagory;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       // View::share('name', 'bitm');
        View::composer('fontPage.layouts.inc.banner', function($view) {
            $publishedCatagories = Catagory::where('publicationStatus', 1)->take(5)->get();
            $view->with('publishedCatagories', $publishedCatagories);
        });
            
 View::composer('fontPage.layouts.inc.header_bot', function($view){ 
          $selectCatagory=Catagory::where('publicationStatus',1)->take(10)->get();
                    $view->with('selectCatagory',$selectCatagory);
    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
