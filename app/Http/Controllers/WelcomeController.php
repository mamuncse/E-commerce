<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catagory;
use App\Product;
class WelcomeController extends Controller
{
    public function index(){
        $publishedProducts=Product::where('publicationStatus',1)->paginate(9);
              
                
                
        return view('fontPage.index',['publishedProducts'=>$publishedProducts]);
    }
    public function catagory($id){

          $publishedCatagoryProducts=Product::where('catagoryId',$id)
                  ->where('publicationStatus',1)->skip(0)->take(8)
                  ->get();
        return view('fontPage.catagory.catagoryContaint',['publishedCatagoryProducts'=>$publishedCatagoryProducts]);
    }
   
    public function contact(){
        return view('fontPage.contact.contact');
    }
    public function productDetails($id){
         $productById = Product::where('id',$id)->first();
        return view('fontPage.product_details.productContent',['productById'=>$productById]);
    }
//    public function paginateCatagory(){
//    $allCatagory= Catagory::paginate(5);
//     return view('fontPage.catagory.catagoryContaint',['allCatagory'=>$allCatagory]);
//}
}
