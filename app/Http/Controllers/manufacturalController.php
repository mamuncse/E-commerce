<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufactural;
use DB;
use Redirect;
class manufacturalController extends Controller
{
    public function createManufactural(){
        return view('admin.Manufactural.index');
    }
    public function storeManufactural(Request $request){
        $this->validate($request, [
           'manufacturalName' =>'required',
            'manufacturalDescription'=>'required',
        ]);
        //return $request->all();
        $manufactural=new Manufactural();
        $manufactural->manufacturalName=$request->manufacturalName;
        $manufactural->manufacturalDescription=$request->manufacturalDescription;
        $manufactural->publicationStatus=$request->publicationStatus;
        $manufactural->save();
        return redirect()->back()->with('message','Manufactural Info Successfully Added!!');
    }
    public function manageManufactural(){
        $manufacturals=Manufactural::all();
        return view('admin.Manufactural.manage',['manufacturals'=>$manufacturals]);
    }
    public function editManufactural($id){
        $manufacturalById=Manufactural::where('id',$id)->first();
        return view('admin.Manufactural.edit',['manufacturalById'=>$manufacturalById]);
    }
    public function updateManufactural(Request $request){
         $manufactural=Manufactural::find($request->id);
        $manufactural->manufacturalName=$request->manufacturalName;
        $manufactural->manufacturalDescription=$request->manufacturalDescription;
        $manufactural->publicationStatus=$request->publicationStatus;
        $manufactural->save();
        return redirect()->back()->with('message','Manufactural Info Successfully Updated!!');
    }
    public function deleteManufactural($id){
          $manufactural=Manufactural::find($id);
          $manufactural->delete();
          return redirect()->back()->with('message','Manufactural Info Successfully Deleted!!');
    }
    public function publishedManufactural($manufatural_id){
    DB::table('manufacturals')
                ->where('id', $manufatural_id)
                ->update(['publicationStatus' => 0]);
        return Redirect::to('/manufactural/manage');
    }

    public function unpublishedManufactural($manufatural_id) {
        DB::table('manufacturals')
                ->where('id', $manufatural_id)
                ->update(['publicationStatus' =>1]);
        return Redirect::to('/manufactural/manage');
    }
}
