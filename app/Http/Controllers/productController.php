<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Catagory;
use App\manufactural;
use Image;
use Redirect;
class productController extends Controller
{
    public function createProduct(){
        $catagories=Catagory::where('publicationStatus',1)->get();
        $manufacturals=manufactural::where('publicationStatus',1)->get();
        return view('admin.Product.menu',['catagories'=>$catagories,'manufacturals'=>$manufacturals]);
    }
    public function storeProduct(Request $request){
       // return $request->all();
        $this->validate($request, [
            'productName' => 'required|max:50',
            'productQuantity' => 'required',
            'productPrize' => 'required',
            'productImage' => 'required',
        ]);
     
     //  $productImage=$request->file('productImage');
//      $imageExtension=$productImage->getClientOriginalExtension();
//       $imageName= strtolower($request->productName).'.'.$imageExtension;
//       return $imageName;
//       $uploadPath='public/productImage/';
//       $productImage->move($uploadPath, $imageName);
//       $imageUrl=$uploadPath.$imageName;
//       $this->saveProductInfo($request,$imageUrl);
        $productImage = $request->file('productImage');
        $imageExtention = $productImage->getClientOriginalExtension();
        $uploadPath ='public/public/productImage/';
        $imageName = $request->productName . '.' . $imageExtention;
         $imageUrl = $uploadPath.$imageName;
          $this->saveProductInfo($request,$imageUrl);
        Image::make($productImage)->resize(255, 291)->save($uploadPath.$imageName);
        return redirect('/product/menu')->with('message', 'Product Info Successfully');
    }

    protected function saveProductInfo($request, $imageUrl) {
        $product = new Product();
        $product->productName = $request->productName;
        $product->catagoryId = $request->catagoryId;
        $product->manufacturalId = $request->manufacturalId;
        $product->productPrize = $request->productPrize;
        $product->productQuantity = $request->productQuantity;
        $product->productShortDescription = $request->productShortDescription;
        $product->productLongDescription = $request->productLongDescription;
        $product->productImage = $imageUrl;
        $product->publicationStatus = $request->publicationStatus;
        $product->save();
    }

    public function manageProduct(){
    $products=DB::table('products')
            ->join('catagories','products.catagoryId','=','catagories.id')
            ->join('manufacturals','products.manufacturalId','=','manufacturals.id')
           ->select('products.id','products.productName','products.productPrize','products.productQuantity','products.publicationStatus','catagories.catagoryName', 'manufacturals.manufacturalName')
            ->get();
        return view('admin.Product.manage',['products'=>$products]);
    }
    public function viewsProduct($id){
         $productById = DB::table('products')
                ->join('catagories', 'products.catagoryId', '=', 'catagories.id')
                ->join('manufacturals', 'products.manufacturalId', '=', 'manufacturals.id')
                ->select('products.*', 'catagories.catagoryName', 'manufacturals.manufacturalName')
                ->where('products.id', $id)
                ->first();

        return view('admin.Product.viewProduct', ['product' => $productById]);
    }
    public function editProduct($id){
//          $editProduct = DB::table('products')
//                ->join('catagories', 'products.catagoryId', '=', 'catagories.id')
//                ->join('manufacturals', 'products.manufacturalId', '=', 'manufacturals.id')
//                ->select('products.*', 'catagories.catagoryName', 'manufacturals.manufacturalName')
//                ->where('products.id', $id)
//                ->first();
           $catagories = Catagory::where('publicationStatus', 1)->get();
        $manufacturals = manufactural::where('publicationStatus', 1)->get();
        $productById = Product::where('id', $id)->first();

        return view('admin.Product.edit')
                        ->with('productById', $productById)
                        ->with('catagories', $catagories)
                        ->with('manufacturals', $manufacturals);
    }
    public function updateProduct(Request $request){
        $imageUrl = $this->imageExists($request);
     
     
    }
    private function imageExists($request){
         $productById = Product::where('id',$request->productId)->first(); 
         $productImage=$request->file('productImage'); 
        // $product =Product::find($request->id);
          $productById->productName = $request->productName;
        $productById->catagoryId = $request->catagoryId;
        $productById->manufacturalId = $request->manufacturalId;
        $productById->productPrize = $request->productPrize;
        $productById->productQuantity = $request->productQuantity;
        $productById->productShortDescription = $request->productShortDescription;
        $productById->productLongDescription = $request->productLongDescription;
        $productById->productImage =$imageUrl;
        $productById->publicationStatus = $request->publicationStatus;
        $productById->save();
         
         if ($productImage) {
             unlink($productById->productImage);
            $imageName = $productImage->getClientOriginalName();
            $uploadPath = 'public/productImage/';
            $productImage->move($uploadPath, $imageName);
            $imageUrl = $uploadPath.$imageName;
        }else{
            
              $imageUrl=$productById->productImage;
         }
         return $imageUrl;
          
    }
    public function deleteProduct($id) {
        $product = Product::find($id);
        $product->delete();
        return redirect()->back()->with('message', 'product Successfully Deleted');
    }
        public function publishedProduct($product_id){
    DB::table('products')
                ->where('id', $product_id)
                ->update(['publicationStatus' => 0]);
        return Redirect::to('/product/manage');
    }

    public function unpublishedProduct($product_id) {
        DB::table('products')
                ->where('id', $product_id)
                ->update(['publicationStatus' =>1]);
        return Redirect::to('/product/manage');
    }
}
