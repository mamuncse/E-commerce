<?php

namespace App\Http\Controllers;

use App\Catagory;
use Illuminate\Http\Request;
use DB;
use Redirect;
class catagoryController extends Controller
{
public function createCatagory(){
    return view('admin.Catagory.index');
}
public function storeCatagory(Request $request){
    $this->validate($request,
            [
                'catagoryName'=>'required',
                'catagoryDescription'=>'required',
            ]
            );
     //return $request->all();
    // $catagory=new Catagory();
    // $catagory->catagoryName=$request->catagoryName;
    // $catagory->catagoryDescription=$request->catagoryDescription;
    // $catagory->publicationStatus=$request->publicationStatus;
    // $catagory->save();
    // return 'Catagory Info Save Successfully';
   // Catagory::create($request->all());
   // return 'Catagory Info Save Successfully';
	DB::table('catagories')->insert(['catagoryName'=>$request->catagoryName,
		'catagoryDescription'=>$request->catagoryDescription,
		'publicationStatus'=>$request->publicationStatus,
		]);
	return redirect()->back()->with('message','Catagory Info Save Successfully');
}
public function manageCatagory(){
    $catagories=Catagory::all();
    return view('admin.Catagory.manage',['catagories'=>$catagories]);
}
public function editCatagory($id){
      $catagoriesById=Catagory::where('id', $id)->first();
    return view('admin.Catagory.edit',['catagoriesById'=>$catagoriesById]);
}
public function updateCatagory(Request $request){
   // dd($request->all());
    $catagory= Catagory::find($request->id);
    $catagory->catagoryName=$request->catagoryName;
    $catagory->catagoryDescription=$request->catagoryDescription;
    $catagory->publicationStatus=$request->publicationStatus;
    $catagory->save();
    return redirect()->back()->with('message','Catagory Successfully updated');
            
}
public function deleteCatagory($id){
 $catagory= Catagory::find($id);
 $catagory->delete();
  return redirect()->back()->with('message','Catagory Successfully Deleted');
}
public function publishedCatagory($catagory_id){
    DB::table('catagories')
                ->where('id', $catagory_id)
                ->update(['publicationStatus' => 0]);
        return Redirect::to('/catagory/manage');
    }

    public function unpublishedCatagory($catagory_id) {
        DB::table('catagories')
                ->where('id', $catagory_id)
                ->update(['publicationStatus' =>1]);
        return Redirect::to('/catagory/manage');
    }
}
