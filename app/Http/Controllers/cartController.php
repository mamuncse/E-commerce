<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use DB;
class cartController extends Controller
{
    public function cartDetails($id,Request $request=NULL){
        $product_info = DB::table('products')->where('id', $id)->first();
       
        $data['id'] = $product_info->id;
        $data['name'] = $product_info->productName;
        $data['price'] = $product_info->productPrize;
        $qty=$request->qty;
        if($qty>1){
            $data['qty']=$qty;
        }else{
            $data['qty']=1;
        }
   
        $data['options'] = array(
            'image'=>$product_info->productImage
        );
        Cart::add($data);
         return view('fontPage.Cart.cartview');
    }
    public function cartDelete($rowId){
        Cart::remove($rowId);
      return view('fontPage.Cart.cartview');
    }
    public function cartUpdate(Request $request){
        $rowId = $request->rowId;
        $qty = $request->qty;
        Cart::update($rowId, $qty);
        return view('fontPage.Cart.cartview');
    }
}
