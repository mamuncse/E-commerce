<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer;
use Session;
use App\Shipping;
use App\Order;
use App\OrderDetail;
use App\Payment;
use Cart;
class checkoutController extends Controller
{
    public function index(){
        return view('fontPage.Checkout.checkContent');
    }


    public function customerRegister(Request $request){
        $this->validate($request,
                [
                    'fname'=>'required',
                    'email'=>'required',
                    'password'=>'required',
                    'phone'=>'required',
                    'address'=>'required',
                    'district'=>'required',
                    
                ]
                );
        $customer = new Customer();
        $customer->fname = $request->fname;
        $customer->email = $request->email;
        $customer->password = bcrypt($request->password);
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->district = $request->district;
        $customer->save();
        $customerId=$customer->id;
        Session::put('customerId',$customerId);
        return redirect('/checkout/shipping');
    }
    public function showShipping(){
        $customerId=Session::get('customerId');
       $customerById=Customer::where('id',$customerId)->first();
          return view('fontPage.Checkout.shippingContent',['customerById'=>$customerById]);
    }
    public function shippingRegister(Request $request){
        $shipping=new Shipping();
        $shipping->fname = $request->fname;
        $shipping->email = $request->email;
        $shipping->phone = $request->phone;
        $shipping->address = $request->address;
        $shipping->district = $request->district;
        $shipping->save();
        $shippingId=$shipping->id;
        Session::put('shippingId',$shippingId);
        return redirect('/checkout/payment');
        
    }
    public function showPaymentForm(){
         return view('fontPage.Checkout.paymentContent');
    }
    public function saveOrderInfo(Request $request){
        $paymentType=$request->paymentType;
        if($paymentType=='CashOnDelivary'){
            $order=new Order();
            $order->customerId=Session::get('customerId');
            $order->shippingId=Session::get('shippingId');
            $order->orderTotal=Session::get('orderTotal');
            $order->save();
            $orderId=$order->id;
            Session::put('orderId',$orderId);
            
            $payment=new Payment();
            $payment->orderId=Session::get('orderId');
            $payment->paymentType=$paymentType;
            $payment->save();
          
            $orderDetail=new OrderDetail();
             $contents = Cart::content();
             foreach ($contents as $content){
            $orderDetail->orderId = Session::get('orderId');
            $orderDetail->productId = $content->id;
            $orderDetail->productName = $content->name;
            $orderDetail->productPize = $content->price;
             $orderDetail->productQuantity = $content->qty;
             $orderDetail->save();
            Cart::remove($content->rowId);
             }
             return redirect('/checkout/my-home');
        }elseif ($paymentType=='Bkash') {
           
            return 'under construction paypal payment type.plz use cash On Delivary';
        }elseif ($paymentType=='Paypal') {
            return 'under construction paypal payment type.plz use cash On Delivary';
        }
    }
    public function customerHome(){
       return view('fontPage.Checkout.customerHome');  
    }
}
